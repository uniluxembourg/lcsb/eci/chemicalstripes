import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib.patches import Rectangle
from matplotlib.collections import PatchCollection
from matplotlib.colors import ListedColormap
import requests
from io import StringIO
from datetime import datetime

def fetch_data(url):
    response = requests.get(url)
    response.raise_for_status()
    data = pd.read_csv(StringIO(response.text))
    return data

def plot_chemical_stripes(pc_id, date_range=(1900, 2024), mode='patents', colorblind=True):
    if mode == 'patents':
        url = f"https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/{pc_id}/PatentCountsByYear/CSV"
    elif mode == 'literature':
        url = f"https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/{pc_id}/LiteratureCountsByYear/CSV"
    else:
        raise ValueError("Invalid mode. Choose either 'patents' or 'literature'.")
    
    data = fetch_data(url)
    data = data[data['Count'] != 'NULL']
    data['Count'] = data['Count'].astype(int)
    
    if mode == 'patents':
        data = data.rename(columns={'Year': 'year'})
        firstyear_col = 'year'
    else:
        data = data.rename(columns={'Year': 'year'})
        firstyear_col = 'year'

    firstyear = data[firstyear_col].min()
    firstyear_date = datetime(firstyear, 1, 1)
    
    all_years = pd.DataFrame({'year': range(date_range[0], date_range[1] + 1)})
    data_by_year = pd.merge(all_years, data, on='year', how='left').fillna(0)

    FIRST = date_range[0]
    LAST = date_range[1]
    
    counts = data_by_year['Count']
    
    if colorblind:
        colors = sns.color_palette("RdYlBu_r", 256)  # Using reversed RdYlBu for correct order
    else:
        colors = plt.cm.viridis(range(256))

    cmap = ListedColormap(colors)

    #fig = plt.figure(figsize=(15, 3))
    #ax = fig.add_subplot(111)

    fig, ax = plt.subplots(figsize=(15, 3))

    # Add x-axis labels
    ax.set_xticks(range(FIRST, LAST + 1, 6))
    ax.set_xticklabels(range(FIRST, LAST + 1, 6), rotation=90)
    ax.set_yticks([])

    col = PatchCollection([
        Rectangle((year, 0), 1, 1) for year in range(FIRST, LAST + 1)
    ])
    
    col.set_array(counts)
    col.set_cmap(cmap)
    col.set_clim(0, counts.max())
    ax.add_collection(col)

    ax.set_ylim(0, 1)
    ax.set_xlim(FIRST, LAST + 1)

    # Add color bar
    cbar = fig.colorbar(col, orientation='vertical', fraction=0.02, pad=0.04)
    cbar.ax.set_ylabel('Count', rotation=90)

    # Add vertical line at the first year
    ax.axvline(x=firstyear, linestyle='--', color='black')

    plt.tight_layout(pad=2)  # Adjust layout to make room for the color bar
    plt.show()

# Example usage:
plot_chemical_stripes(1234, mode='patents')