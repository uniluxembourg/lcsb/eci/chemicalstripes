# R Package to generate your own chemical stripes
## by Dagny Aurich
Creates a stripe plot showing the number of patents filed or references published for a given compound (or a list of compounds) over time, based on the famous [warming stripes](https://showyourstripes.info/s/globe).

Patent and consolidate reference numbers retrieved from PubChem using the [PUGREST](https://pubchem.ncbi.nlm.nih.gov/docs/pug-rest) interface, 
see the preprint Aurich et al. DOI: [10.26434/chemrxiv-2024-6jkxv](https://doi.org/10.26434/chemrxiv-2024-6jkxv) for details.

Note: The default patent date is set to 2022 as the latest data in PubChem is currently from Dec. 2022.

## How To: 
Install 'chemicalStripes' from GitLab via

devtools::install_git("https://gitlab.com/uniluxembourg/lcsb/eci/chemicalstripes.git")

Dependencies:

    webchem [>=1.2.0]
  
    vroom [>=1.6.1]
    
    knitr [>=1.4.2]
    
    tidyverse [>=2.0.0]
    
    lubridate [>=1.9.2]
    
    RColorBrewer [>=1.1-3]
    
    progress [>=1.2.2]
    
    scales [>=1.2.1]
    
    assertthat [>=0.2.1]

Default functions: 
- **chemical_stripes(pc_id, date_range, mode = "patents", colorblind = FALSE)**

- **sum_stripes(pc_id, date_range, colorblind = FALSE, description)**

**pc_id**: 
- chemical_stripes: Insert a numeric PubChem Chemical Identifier from [PubChem](https://pubchem.ncbi.nlm.nih.gov/) 

- sum_stripes: Insert up to 300 numeric PubChem Chemical Identifiers from [PubChem](https://pubchem.ncbi.nlm.nih.gov/) 

**date_range**: Insert a numeric vector of length 2 specifying the range of years to display (default: c(1960,2022))

**mode**: Insert either "literature" or "patents" to define the data source (default: "patents")

**colorblind**: Select TRUE/FALSE to receive a colorblind friendly 'Blue-Yellow-Red' version of the stripes. 

**description**: Select a  plot title AND filename to save the stripes plot later. Don't use special characters. This option just applies to the **sum_stripes** function.

The output is a ggplot object (automatically saved as png) displaying the number of patents/references filed/published for the compound (list) over time.


## About the Stripes:

# Chemical Stripes – Visualizing Chemical Trends of the Past Influencing Today

## Authors:

Dagny Aurich<sup>1</sup>, Hans Peter Arp<sup>2,3</sup>, Sarah Hale<sup>2</sup>, Kerry Sims<sup>4</sup>, Emma L. Schymanski<sup>1</sup> 

1 Luxembourg Centre for Systems Biomedicine (LCSB), University of Luxembourg, Avenue du Swing 6, L-4367 Belvaux, Luxembourg

2 Norwegian Geotechnical Institute (NGI), P.O. Box 3930, Ullevål Stadion, 0806 Oslo, Norway

3 Department of Chemistry, Norwegian University of Science and Technology (NTNU), 7491, Trondheim, Norway

4 Environment Agency, Horizon House, Deanery Road, Bristol, BS1 5AH  

E-mail contact:   [dagny.aurich@uni.lu](mailto:dagny.aurich@uni.lu)

## 1.	Introduction
The investigation of historical chemical exposures can provide valuable insights into how the past impacts human health today or even into the future. Moreover, looking at evolving chemical numbers and regulation of (potentially) toxic substances in the environment shows that the need for quick(er) action and prevention is now more urgent than ever. A very useful resource to look at past chemical exposure is patent data covering mainly industrial applications, e.g. to be found via the World Intellectual Property Organization (WIPO).<sup>1</sup> Combining data on increasing chemical numbers in databases like the Chemical Abstract Service (CAS) registry,<sup>2,</sup> the filing dates of patents and the timespan between the starting of discussions on restricting a chemical and the date of regulations entering into force, can help answer questions about whether a change in numbers is observed following regulation and whether regulation could (and should) have come earlier. 
The use of visualization can support the interpretation of this data by showing time trends of chemical or patent numbers and to help identify possible changes due to regulatory measures. Many scientific findings gain value and become more understandable when there is an appropriate visualization accompanying them. In this work a modification of the well-known warming<sup>3</sup> or biodiversity stripes<sup>4</sup> is used to visualize the patent data of chemicals relevant to planetary boundary threats, such as persistent and mobile substances.<sup>5</sup> The increase in overall numbers of chemicals in databases (CAS registry: 202 million chemical substances in November 2022) and even in numbers of restricted chemicals is alarming. Using these visualizations is a possible way to raise awareness to this problem.

## 2.	Materials and Methods
The warming or climate stripes were first used to visualize temperature trends over the years using a colour scheme from blue (cold) to red (warm). The graphics have been extended with different kinds of environmental data, e.g. sea levels and show a good example of effective science communication. The automated extraction of patent data from the PubChem classification browser for chemical groups of concern - being known for their persistence in the environment - aids as a basis of visualizing past and present chemical trends. Using the model of chemical stripes evolving from small numbers in green to bigger ones in yellow to large ones in red, i.e., a traffic light pattern, helps show the alarming (red) state of the present. The patent data can be complemented by adding regulation dates, e.g. from the Stockholm Convention<sup>6</sup> and deductions can be drawn comparing it to the overall number of chemicals registered in databases such as CAS.
  

## 3.	Results and Discussion
The overall trend of rising chemical numbers in databases mirrors the increasing numbers of chemical compounds in the environment today. However, looking at specific chemical classes that are relevant to planetary boundary threats, this trend is alarming. Even after regulation of certain compounds the patent numbers, i.e. the industrial applications rise and therefore potentially also their presence in the environment. The time frame between the event of suspecting a chemical to be a threat and the regulatory action taking place is often several decades, e.g. for perfluorooctanoic acid (PFOA) regulations came into force 50 years after DuPont discovered the harming compound in the blood of workers and 20 years after multiple lawsuits were filed against the company.<sup>7</sup> Many compounds detected in the environment are classified as being persistent and mobile, and thus are not removed easily from the soil or water bodies and therefore rapidly accumulating. Looking at specific substance classes can raise awareness of what went wrong in the past or what actions could have been taken earlier in order to improve regulation in the future. 
All this information can be collected and visualized in the chemical stripes graphics to present the need for action in a very understandable way. The stripes for nearly all substance classes show the same trend: A drastic increase of numbers of potentially threatening chemicals in the environment we live in today. Although the climate stripes were criticized as being too minimalistic in reducing global warming trends to a change in colour, they were well suited to spreading the message to non-scientists and have been widely adopted. The same potential applies to the chemical stripes.<sup>8</sup> While they seem to be reductive, they help deliver a clear and alarming message and can be created for different sets of chemicals. 

## 4.	Conclusions
Past chemical exposure and trends in chemical numbers can be accessed using suitable data visualization techniques. The use of chemical stripes as a modification of the climate stripes graphics gives a possible solution to support understandable science communication. Possible ways to extend the visualization are endless, e.g. adding regulation dates. In a minimalistic way the stripes show the need for action to eliminate and prevent environmental chemical threats and the urgent need for quicker regulation decisions. 

## 5.	References
1.	WIPO - World Intellectual Property Organization. Accessed October 24, 2022. https://www.wipo.int/
2.	CAS REGISTRY. CAS. Accessed August 2, 2021. https://www.cas.org/cas-data/cas-registry
3.	2018 visualisation update | Climate Lab Book. Accessed October 25, 2022. https://www.climate-lab-book.ac.uk/2018/2018-visualisation-update/
4.	Miles. Biodiversity Stripes – A Journey from Green to Grey. Finding Nature. Published August 10, 2022. Accessed October 24, 2022. https://findingnature.org.uk/2022/08/10/biodiversity-stripes/
5.	Persson L, Carney Almroth BM, Collins CD, et al. Outside the Safe Operating Space of the Planetary Boundary for Novel Entities. Environ Sci Technol. 2022;56(3):1510-1521. doi:10.1021/acs.est.1c04158
6.	Stockholm Convention - Home page. Accessed October 25, 2022. http://www.pops.int/
7.	‘Dark Waters’ and PFOA – FAQ. CHEM Trust. Published February 26, 2020. Accessed November 14, 2022. https://chemtrust.org/dark-waters-and-pfoa-faq/
8. Arp HPH, Aurich D, Schymanski EL, Sims K, Hale SE. Avoiding the Next Silent Spring: Our Chemical Past, Present, and Future. Environ Sci Technol. Published online April 13, 2023:acs.est.3c01735. doi:10.1021/acs.est.3c01735


## Acknowledgement 

The authors thank Egon Willighagen, Jeremy Frey and all LuxTime and ECI colleagues who participated in the discussion and gave input on the ‘chemical stripes’ visualization. We would also like to acknowledge Jennifer Yang (CAS registry numbers) and Paul Thiessen & Evan Bolton (PubChem, WIPO patent numbers) for providing the data and/or tips to retrieve the data used. Funding is acknowledged from the University of Luxembourg Institute for Advanced Studies Audacity Program for the Luxembourg Time Machine project LuxTime, from the Luxembourg National Research Fund (FNR) for project A18/BM/12341006 and from the European Union Horizon 2020 research and innovation programme under Grant Agreement No 101036756, ZeroPM. 
